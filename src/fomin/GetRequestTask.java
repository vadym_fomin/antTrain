package fomin;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Vadim on 06.07.2017.
 */
public class GetRequestTask extends Task {

    private static final String RESOURCE_URL = "http://www.google.com";

    private static final String REQUEST_METHOD = "GET";

    @Override
    public void execute() throws BuildException {
        URL obj = null;
        HttpURLConnection connection = null;
        try {
            obj = new URL(RESOURCE_URL);
            connection = (HttpURLConnection) obj.openConnection();
            connection.setRequestMethod(REQUEST_METHOD);
            System.out.println("Response code: " + connection.getResponseCode());
            System.out.println("Response message: " + connection.getResponseMessage());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }
    }
}
